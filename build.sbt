name := "pierce"

version := "0.1"

scalaVersion := "2.13.1"

libraryDependencies ++= Seq(
  "com.lihaoyi" %% "fastparse" % "2.1.3",

  "org.scalatest" %% "scalatest" % "3.1.0" % "test",
)

