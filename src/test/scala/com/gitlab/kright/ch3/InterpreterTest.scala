package com.gitlab.kright.ch3

import com.gitlab.kright.ch3.UntypedParser._

class InterpreterTest extends org.scalatest.FunSuite {

  test("primitive values") {
    for (term <- Seq(True, False, Zero)) {
      assert(Interpreter(term) == term)
    }
  }

  test("isZero") {
    for {
      zero <- Seq(
        Zero,
        Succ(Pred(Zero)),
        IfExpr(True, Zero, True)
      )
    } {
      assert(Interpreter(IsZero(zero)) == True)
    }

    for {
      nonZero <- Seq(
        True,
        False,
        Succ(Zero),
        Pred(Zero),
        IfExpr(True, True, False)
      )
    } {
      assert(Interpreter(IsZero(nonZero)) == False)
    }
  }

  test("succ pred reduction") {
    case class Gen(level: Int, constructor: Term => Term)

    val gens = Seq(Gen(1, Succ), Gen(-1, Pred))

    for {
      g1 <- gens
      g2 <- gens
      g3 <- gens
      g4 <- gens
    } {
      val all = Seq(g1, g2, g3, g4)

      val term = all.foldLeft(Zero: Term) { case (term, g) => g.constructor(term) }
      val level = all.map(_.level).sum

      level match {
        case -4 => assert(Interpreter(term) == Pred(Pred(Pred(Pred(Zero)))))
        case -2 => assert(Interpreter(term) == Pred(Pred(Zero)))
        case -0 => assert(Interpreter(term) == Zero)
        case 2 => assert(Interpreter(term) == Succ(Succ(Zero)))
        case 4 => assert(Interpreter(term) == Succ(Succ(Succ(Succ(Zero)))))
      }
    }
  }

  test("if cond") {
    assert(Interpreter(IfExpr(True, True, False)) == True)
    assert(Interpreter(IfExpr(False, True, False)) == False)
  }

}
