package com.gitlab.kright.ch3

import com.gitlab.kright.ch3.UntypedParser._

class UntypedParserTest extends org.scalatest.FunSuite {

  test("parse true") {
    assert(parse("true") == True)
  }

  test("parse false") {
    assert(parse("false") == False)
  }

  test("parse 1") {
    assert(parse("succ 0") == Succ(Zero))
  }

  test("parse 0") {
    assert(parse("isZero succ pred 0") == IsZero(Succ(Pred(Zero))))
  }

  test("parse if expr") {
    assert(parse("if true then false else iszero 0") == IfExpr(True, False, IsZero(Zero)))
  }

}
