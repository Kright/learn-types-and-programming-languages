package com.gitlab.kright.ch3

import com.gitlab.kright.ch3.UntypedParser._

object Interpreter {
  def apply(term: Term): Term =
    term match {
      case True => True
      case False => False
      case Zero => Zero
      case IsZero(innerTerm) =>
        apply(innerTerm) match {
          case Zero => True
          case _ => False
        }
      case IfExpr(cond, branch1, branch2) =>
        apply(cond) match {
          case True => apply(branch1)
          case False => apply(branch2)
          case _ => ???
        }
      case Succ(innerTerm) =>
        apply(innerTerm) match {
          case Zero => Succ(Zero)
          case Succ(nested) => Succ(Succ(nested))
          case Pred(nested) => nested
          case _ => ???
        }
      case Pred(innerTerm) =>
        apply(innerTerm) match {
          case Zero => Pred(Zero)
          case Succ(nested) => nested
          case Pred(nested) => Pred(Pred(nested))
        }
    }
}
