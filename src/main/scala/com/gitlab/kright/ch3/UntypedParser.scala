package com.gitlab.kright.ch3

import fastparse.MultiLineWhitespace._
import fastparse.{P, _}

object UntypedParser {

  sealed trait Term

  case object True extends Term

  case object False extends Term

  case object Zero extends Term

  case class Succ(t: Term) extends Term

  case class Pred(t: Term) extends Term

  case class IsZero(t: Term) extends Term

  case class IfExpr(cond: Term, branch1: Term, branch2: Term) extends Term

  def pTrue[_: P]: P[Term] = P("true").map(_ => True)

  def pFalse[_: P]: P[Term] = P("false").map(_ => False)

  def pZero[_: P]: P[Term] = P("0").map(_ => Zero)

  def pSucc[_: P]: P[Term] = P("succ" ~/ pTerm).map(Succ)

  def pPred[_: P]: P[Term] = P("pred" ~/ pTerm).map(Pred)

  def pIsZero[_: P]: P[Term] = P(("iszero" | "isZero") ~/ pTerm).map(IsZero)

  def pIfExpr[_: P]: P[Term] = P("if" ~/ pTerm ~/ "then" ~/ pTerm ~/ "else" ~/ pTerm).map {
    case (cond, branch1, branch2) => IfExpr(cond, branch1, branch2)
  }

  def pTerm[_: P]: P[Term] = P(pTrue | pFalse | pZero | pSucc | pPred | pIsZero | pIfExpr)

  def parse(text: String): Term = {
    val Parsed.Success(term, _) = fastparse.parse(text, pTerm(_))
    term
  }

  def apply(text: String): Term = parse(text)
}
