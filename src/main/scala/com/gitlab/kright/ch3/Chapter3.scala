package com.gitlab.kright.ch3

object Chapter3 extends App {

  def run(text: String): Unit = println(Interpreter(UntypedParser(text)))

  run("succ pred 0")
  run("true")
  run("isZero 0")
  run("succ pred pred succ succ succ 0")
  run("if isZero succ pred pred 0 then pred succ 0 else true")
}
